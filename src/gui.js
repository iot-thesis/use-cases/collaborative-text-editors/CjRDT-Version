/*
 * Front-end of the collaborative text editor.
 * Communicates updates to the TextEditor CRDT that runs on the back-end, through HTML5 WebSockets.
 */

var doc = [];
var currentInput = "";
var socket;
var backEndPort = 8080;
var connected = false;
var input;

// On page load
onload = function() {
    
    /*
     * Setup communication with the text editor's back-end.
     */

    if ("WebSocket" in window) {
        socket = new WebSocket("ws://127.0.0.1:" + backEndPort);
        
        socket.onopen = function() {
            connected = true;
            flushBuffer();
        };
        
        socket.onmessage = onMessage;
        
        socket.onclose = function() {
            connected = false;
            //toastr.warning('Lost connection with the back-end');
            console.warn('Lost connection with the back-end.');
        };
    }
    else {
        console.error('Your browser does not support WebSocket!');
    }
    
    input = document.getElementById('docText');
    
    input.onkeyup = handleChanges.bind(null, () => currentInput, (x) => currentInput = x, input);
    
    var clear = document.getElementById('clear');
    clear.onclick = () => sendToBackEnd(message('clearDocument'));
};

function updateTextfield(textfield, setCurrentString, content) {
    // Accumulate the visible characters
    setCurrentString(content);
    textfield.value = content;
}

function handleChanges(getOldString, setCurrentString, textfield, e) {
    var old     = getOldString();
    var current = textfield.value;
    var idx     = textfield.selectionStart; // works because we do not allow selections (only adding/removing one character at a time)
    var end     = Math.min(old.length, current.length);
    var acceptedCharacters = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?!.&*-_/:;,+=$@#".split('');
    
    if (e.keyCode == 46 || e.keyCode == 8) {
        // Deletion
        //var deletedCharacter = doc.filter(char => char.visible)[idx];
        var deleteMsg = message('delete', idx);
        sendToBackEnd(deleteMsg);
    }
    else if (acceptedCharacters.includes(String.fromCharCode(e.keyCode))) {
        // Addition
        idx -= 1;
        var addedCharacter = current.charAt(idx);
        //var previousCharacterID = (idx === 0) ? DOC_START : doc.filter(char => char.visible)[idx-1].id;
        
        var insertAfterMsg = message('insertAfter', idx, addedCharacter);
        sendToBackEnd(insertAfterMsg);
    }
    else {
        textfield.value = old; // remove the invalid character
        console.warn(`Ignored character ${String.fromCharCode(e.keyCode)}`);
        return;
    }
    
    setCurrentString(current);
}

function onMessage(msg) {
    msg = JSON.parse(msg.data);
    if (msg.type === 'documentUpdate') {
        doc = msg.content;
        updateTextfield(input, (x) => currentInput = x, doc);
    }
    else {
        console.warn('Did not understand message from back-end.');
    }
}

function message(operation, ...args) {
    return { operation: operation, args: args };
}

var buffer = [];
function sendToBackEnd(msg) {
    if (connected)
        socket.send(JSON.stringify(msg));
    else
        buffer.push(msg);
}

function flushBuffer() {
    buffer.forEach(sendToBackEnd);
    buffer = [];
}
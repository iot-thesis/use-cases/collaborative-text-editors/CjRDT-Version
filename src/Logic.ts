/*
 * Implementation of a collaborative text editor.
 * A text document is a list of characters.
 *
 * Supported operations:
 *  - insertAfter(idx, char) --> void
 *  - delete(idx) --> void
 *  - clearDocument() --> void
 *  - getContent() --> string
 */

import cjrdt from "cjrdt";

class TextEditor {
    constructor(initialContent: string = "") {
        cjrdt.reset();
        cjrdt.doc = initialContent.split('');
    }

    getContent(): string {
        return cjrdt.doc.toJS().reduce((str, char) => str + char, "");
    }

    // Inserts one or more characters at a certain position in the document
    insertAfter(idx: number, chars: string) {
        var elem = cjrdt.doc.idx(idx);
        for (var i = 0; i < chars.length; i++) {
            const char = chars.charAt(i);
            elem = elem.insertAfter(char);
        }
    }

    delete(idx: number) {
        cjrdt.doc.idx(idx+1).delete(); // idx + 1 because the JSON CRDT starts indexing from 1
    }

    // Wipes out all text contained by the document
    clearDocument() {
        cjrdt.doc = [];
    }
}

const onUpdate = cjrdt.onUpdate;
export { TextEditor, onUpdate };
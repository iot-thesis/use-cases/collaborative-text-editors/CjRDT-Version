/*
 * 1) Run this server
 *     "$(npm bin)"/ts-node -O '{"target": "es6", "allowJs": true}' ./src/Server.ts
 * 2) Open the printed address in your browser
 */

import * as express from "express";
import * as path from "path";
import {TextEditor, onUpdate} from "./Logic";

const app = express();

/*
 * Serve the text editor as a static file.
 */

app.use(express.static(path.join(__dirname)));
app.use('/images', express.static(path.join(__dirname, '..', 'images')));

// Create the text editor
const textEditor = new TextEditor();

// React to changes in the json document
onUpdate(pushUpdate.bind(null, textEditor));

/*
 * Initialize websocket
 */

var WebSocketServer = require('ws').Server,
    port = 8080,
    wss = new WebSocketServer({port: port});

console.log('Websocket server listening at http://127.0.0.1:' + port);

/*
 * Listen for connections and messages
 */

var socket;
var buffer = [];
wss.on('connection', function(sock) {
    socket = sock;
    onSocket(sock);
    socket.on('message', function(msg) {
        msg = JSON.parse(msg);
        if (!msg.operation || !msg.args)
            console.log('Did not understand message coming from the front-end.');
        else
            onMessage(msg);
    });

    socket.on('close', function() {
        socket = null;
    });

    socket.on('error', function(error) {
        console.log('Socket error: ' + error);
        socket = null;
    });
});

function onSocket(s) {
    socket = s;
    flushBuffer();
}

function flushBuffer() {
    buffer.forEach(sendMessage);
    buffer = [];
}

// Pushes the new document content to the front-end
function pushUpdate(editor) {
    var msg = { type: 'documentUpdate', content: editor.getContent() };
    sendMessage(msg);
}

/*
 * Processes received messages
 */

function onMessage({operation, args}) {
    switch (operation) {
        case 'insertAfter':
            var [idx, char] = args;
            textEditor.insertAfter(idx, char);
            break;
        case 'delete':
            var [idx] = args;
            textEditor.delete(idx);
            break;
        case 'clearDocument':
            textEditor.clearDocument();
            break;
        default:
            console.log('Unsupported operation \'' + operation + '\'');
    }
}

function sendMessage(msg) {
    if (socket)
        socket.send(JSON.stringify(msg));
    else
        buffer.push(msg);
}